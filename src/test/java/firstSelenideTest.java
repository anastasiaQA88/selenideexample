import org.testng.annotations.Test;
import pages.baseTestClass;

import static com.codeborne.selenide.Selenide.open;
import static helpers.testData.searchRequest;
import static pages.pageWithSelenide.*;

public class firstSelenideTest extends baseTestClass {

    @Test(description = "Пример 1", priority = 0)
    public void testFirstSelenide() {


        //Открываем страницу Гугла
        openMainPage();

        enterField(searchFieldGoogle, searchRequest);

        //жмем энтер
        pressEnterForElement(searchFieldGoogle);

        //ассерт 1
        checkElementVisible(searchResultsDiv);

        //ассерт 2
        checkElementVisible(searchResultWiki);

    }


}
